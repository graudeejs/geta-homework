(function(window) {

  Vue.component('external-link', {
    template: '<a :href="href" rel="nofollow noreferrer" target="_blank"><slot></slot><a>',
    props: ['href']
  });

  Vue.component('md-collapsible', {
    template: '<ul class="collapsible"><slot></slot></ul>',
    mounted: function() {
      $(this.$el).collapsible();
    }
  });

  Vue.component('gem-dependencies', {
    props: ['dependencies', 'title'],
    template: '#gem-dependencies-template'
  });

  window.app = new Vue({
    el: $('#mainContent')[0],
    data: {
      latestGems: [],
      latestGemsLoaded: false,
      inspectedGem: null
    },
    methods: {
      toggleInspectGem: function(gem) {
        this.inspectedGem = this.inspectedGem == gem ? null : gem;
      }
    },
    mounted: function() {
      var self = this;
      $.ajax({
        type:       'GET',

        // I decided to use rubygems api. I quickly fetched latest.json
        // locally, so I don't have to wait remote server response every time I
        // need to reload browser. However when I switched to real API, I
        // realized that I won't be able to use it, because of CORS. I then
        // decided to simply continue using fetched json, since I had no time
        // to evaluate new api and basically rewrite everything.
        url:        'latest.json',
        // url:        'https://rubygems.org/api/v1/activity/latest.json',

        timeout:    3000,
        crossOrigin: true,
        success:    function (data) {
                      setTimeout(function(){
                        self.latestGems = data;
                        self.latestGemsLoaded = true;
                      }, 500); // just to be able to see progress bar :)
                    },
      });
    }
  });

})(self);
